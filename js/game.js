"use strict";

var cv, ctx;
var FPS = 30, canvasWidth = 800, canvasHeight = 600;
var currentState;

var curDifficulty;
var difficulty = {
    easy: 0.1,
    medium: 0.15,
    hard: 0.2,
};

var curTileSize;
var tileSize = {
    massive: { x: 4, y: 4 },
    large: {x:16, y:10} ,
    normal: { x: 20, y: 12 },
    small: { x: 24, y: 16 },
};

$(document).ready(function () {
    //set difficulty
    curDifficulty = difficulty[$('#difficulty').val()];
    $('#difficulty').change(function () {
        curDifficulty = difficulty[$(this).val()];
        currentState.ExitState();
        currentState = new GameState();
        currentState.InitState();
    });
    //console.log(curDifficulty);
    //set tilesize
    curTileSize = tileSize[$('#tiles').val()];
    $('#tiles').on('change', function () {
        curTileSize = tileSize[$(this).val()];
        currentState.ExitState();
        currentState = new GameState();
        currentState.InitState();
    });
    //console.log(curTileSize);

    //Init some important elements
    cv = document.getElementById("myCanvas");
    ctx = cv.getContext("2d");
    currentState = new GameState();
    currentState.InitState();

    //Disable context
    $('body').on('contextmenu', '#myCanvas', function (e) { return false; });

    //GAME LOOP
    var now,
    dt = 0,
    last = timestamp(),
    step = 1 / FPS;
    function frame() {
        now = timestamp();
        dt = dt + Math.min(1, (now - last) / 1000);
        while (dt > step) {
            dt = dt - step;
            Update(step);
        }
        Draw(dt);
        last = now;
        requestAnimationFrame(frame);
    }
    requestAnimationFrame(frame);

    function Update() {
 
    }

    function Draw() {
        //clear
        ctx.clearRect(0, 0, canvasWidth, canvasHeight);

        //Draw Background
        ctx.fillStyle = "#FFFFFF";
        ctx.fillRect(0, 0, canvasWidth, canvasHeight);

        //draw currentState
        currentState.DrawState();
    }
});