﻿function GameState() {
    var self = this;
    this.menuHeight = 80;
    this.tiles = [];
    this.victory = false;
    this.gameTimer = new Timer();
    this.endTime = 0;
    this.gameOver = false;
    this.restartBtn;
    this.tilesInWidth = curTileSize.x;
    this.tilesInHeight = curTileSize.y;
    this.tileW = canvasWidth / this.tilesInWidth;
    this.tileH = Math.floor((canvasHeight - this.menuHeight) / this.tilesInHeight);
    this.gameOver = false;
    this.flagCount = 0;
    this.mineCount = Math.floor((this.tilesInWidth * this.tilesInHeight) * curDifficulty);
    this.safeTiles = (this.tilesInWidth * this.tilesInHeight) - this.mineCount;

    this.InitState = function () {
        //restart button
        var centerX = (canvasWidth / 2) - (this.menuHeight / 2);
        this.restartBtn = new Button("", centerX, 10, this.menuHeight, this.menuHeight - 20);
        this.restartBtn.SetImage(art.smiley);
        this.restartBtn.OnClicked = function () {
            //console.log("Restart the game");
            currentState.ExitState();
            currentState = new GameState();
            currentState.InitState();
        };

        //create tiles
        this.tiles = new Array(this.tilesInWidth)
        for (var i = 0; i < this.tilesInWidth; i++) {
            this.tiles[i] = new Array(this.tilesInHeight)
            for (var j = 0; j < this.tilesInHeight; j++) {
                this.tiles[i][j] = new Tile();
                this.tiles[i][j].img = art.hidden;
            }
        }

        //Place mines
        for (var i = 0; i < this.mineCount; i++) {
            while(true){
                var x = Math.floor(Math.random() * this.tilesInWidth);
                var y = Math.floor(Math.random() * this.tilesInHeight);
                if (!this.tiles[x][y].mine) {
                    this.tiles[x][y].mine = true;
                    break;
                }
            }
        }

        //Count Mines
        for (var i = 0; i < this.tilesInWidth; i++) {
            for (var j = 0; j < this.tilesInHeight; j++) {
                this.tiles[i][j].neighbours = this.CountNeightbours(i, j);
            }
        }

        //Left click
        cv.addEventListener('click', self.GameLogic, false);
        //Right click
        cv.oncontextmenu = self.GameLogic;

        //Timer
        this.gameTimer.Start(0);
    }

    this.ExitState = function () {
        cv.removeEventListener('click', self.GameLogic, false);
        this.restartBtn.Destroy();
    }

    this.GameLogic = function (e) {

        var rect = cv.getBoundingClientRect();
        var mPos = {};
        mPos.x = e.x - rect.left;
        mPos.y = e.y - rect.top - self.menuHeight;

        //Clicked in grid?
        if (mPos.y < 0)
            return;

        mPos.x = Math.floor(mPos.x / self.tileW);
        mPos.y = Math.floor(mPos.y / self.tileH);

        var t = self.tiles[mPos.x][mPos.y];
        var leftClick = (e.button === 0) ? true : false;
        if (leftClick) {
            if (!t.flag) {
                t.clicked = true;
                if (t.mine)
                    self.GameOver();
                else if (t.neighbours == 0) {
                    t.img = art.open;
                    t.checked = true;
                    self.CheckOpenNeighbours(mPos);
                }
                else
                    t.img = numberArt[t.neighbours];

                self.CheckProgress();
            }
        }
        else { //Right click
            if (t.flag) {
                self.flagCount--;
                t.flag = false;
                t.img = art.hidden;
            }
            else if(!t.flag && !t.clicked && !t.checked )
            {
                self.flagCount++;
                t.flag = true;
                t.img = art.flag;
            }
            self.CheckProgress();
        }

        self.DrawState();
        return false;
    }

    this.CheckProgress = function () {
        var tilesWithMineAndFlag = 0
        for (var i = 0; i < this.tilesInWidth; i++) {
            for (var j = 0; j < this.tilesInHeight; j++) {
                var t = this.tiles[i][j];
                if (t.flag && t.mine) {
                    tilesWithMineAndFlag++;
                }
            }
        }
        if (tilesWithMineAndFlag == this.mineCount && this.flagCount == this.mineCount)
            this.Victory();
    }

    this.Victory = function () {
        this.victory = true;
        this.endTime = Math.floor(this.gameTimer.Progress());
        //console.log("Game finished with time: " + this.endTime);
    }

    this.GameOver = function () {
        this.restartBtn.SetImage(art.dead);
        //Show all mines
        for (var i = 0; i < this.tilesInWidth; i++) {
            for (var j = 0; j < this.tilesInHeight; j++) {
                var t = this.tiles[i][j];
                if (t.mine)
                    t.img = art.mine;
                else if (t.neighbours == 0)
                    t.img = art.open;
                else
                    t.img = numberArt[t.neighbours]
            }
        }
        this.gameOver = true;
    }


    this.DrawState = function () {
        this.restartBtn.Draw();

        var fontSize = 30;
        ctx.font = fontSize + "px Verdana";

        //Mines and flags
        var offset = {x:10,y:20};
        var countWidth = 40;
        ctx.drawImage(art.mine, offset.x, offset.y, countWidth, countWidth);
        ctx.drawImage(art.flag, offset.x*2 + countWidth*4, offset.y, countWidth, countWidth);

        ctx.fillStyle = "#FF0000";

        ctx.fillText(this.mineCount, offset.x*2 + countWidth, offset.y + fontSize);
        ctx.fillText(this.flagCount, offset.x * 3 + countWidth * 5, offset.y+fontSize);

        //Timer
        var timerWidth = 120;
        var timerX = canvasWidth - timerWidth - 10;
        ctx.drawImage(art.timer, timerX, 10, timerWidth, 60);
        ctx.fillStyle = "#FF0000";
        var timeToDisplay = this.victory? this.endTime : Math.floor(this.gameTimer.Progress());
        var textSize = ctx.measureText(timeToDisplay);
        ctx.fillText(timeToDisplay, timerX + (timerWidth/2) - (textSize.width/2), 52);

        //Draw all gameobjects
        for (var i = 0; i < this.tilesInWidth; i++) {
            for (var j = 0; j < this.tilesInHeight; j++) {
                this.tiles[i][j].Draw(i * this.tileW, j * this.tileH + this.menuHeight, this.tileW, this.tileH);
            }
        }

        //Victory
        if (this.victory) {
            //text position
            var text = "Victory";
            fontSize = 120;
            ctx.font = fontSize + "px Verdana";
            var textSize = ctx.measureText(text);
            var textX = (canvasWidth / 2) - (textSize.width / 2);
            var textY = (canvasHeight / 2) - (fontSize / 2);
            ctx.fillStyle = "#FF0000";
            ctx.fillRect(0, textY - fontSize + 10, canvasWidth, fontSize);
            ctx.fillStyle = "#FFFF00";
            ctx.fillText(text, textX, textY);
        }
    }

    this.CheckOpenNeighbours = function (mPos) {
        var tilesToCheck = [];
        tilesToCheck.push(mPos);
        while (tilesToCheck.length > 0) {
            var tile = tilesToCheck[0];

            var startX = (tile.x - 1 < 0) ? tile.x : tile.x - 1;
            var startY = (tile.y - 1 < 0) ? tile.y : tile.y - 1;
            var endX = (tile.x + 1 > (this.tilesInWidth - 1)) ? tile.x : tile.x + 1;
            var endY = (tile.y + 1 > (this.tilesInHeight - 1)) ? tile.y : tile.y + 1;

            for (var i = startX; i <= endX; i++) {
                for (var j = startY; j <= endY; j++) {
                    var curTile = this.tiles[i][j];
                    if (curTile.neighbours == 0 && curTile.checked == false) {
                        var tilePos = { x: i, y: j };
                        tilesToCheck.push(tilePos);
                        curTile.img = art.open;
                    }
                    else if (curTile.neighbours > 0)
                        curTile.img = numberArt[curTile.neighbours];

                    if (curTile.flag) {
                        curTile.flag = false;
                        this.flagCount--;
                    }
                    curTile.checked = true;
                }
            }

            tilesToCheck.splice(0, 1);
        }
    }


    this.CountNeightbours = function (x, y) {
        var count = 0;
        var startX = (x - 1 < 0) ? x : x - 1;
        var startY = (y - 1 < 0) ? y : y - 1;
        var endX = (x + 1 > (this.tilesInWidth - 1)) ? x : x + 1;
        var endY = (y + 1 > (this.tilesInHeight - 1)) ? y : y + 1;

        for (var i = startX; i <= endX; i++) {
            for (var j = startY; j <= endY; j++) {
                if (this.tiles[i][j].mine)
                    count++;
            }
        }
        return count;
    }
};