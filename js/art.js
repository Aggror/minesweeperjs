﻿var numberArt = new Array(9);
numberArt[1] = new Image();
numberArt[2] = new Image();
numberArt[3] = new Image();
numberArt[4] = new Image();
numberArt[5] = new Image();
numberArt[6] = new Image();
numberArt[7] = new Image();
numberArt[8] = new Image();
numberArt[1].src = "images/MINESWEEPER_1.png";
numberArt[2].src = "images/MINESWEEPER_2.png";
numberArt[3].src = "images/MINESWEEPER_3.png";
numberArt[4].src = "images/MINESWEEPER_4.png";
numberArt[5].src = "images/MINESWEEPER_5.png";
numberArt[6].src = "images/MINESWEEPER_6.png";
numberArt[7].src = "images/MINESWEEPER_7.png";
numberArt[8].src = "images/MINESWEEPER_8.png";


var art = {
    timer: new Image(),
    smiley: new Image(),
    dead: new Image(),
    mine: new Image(),
    hidden: new Image(),
    flag: new Image(),
    open: new Image(),
}

art.timer.src = "images/MINESWEEPER_tray.png";
art.smiley.src = "images/bomb-smiley.png";
art.dead.src = "images/bomb-smiley-dead.png";
art.mine.src = "images/MINESWEEPER_M.png";
art.hidden.src = "images/MINESWEEPER_X.png";
art.flag.src = "images/MINESWEEPER_F.png";
art.open.src = "images/MINESWEEPER_0.png";