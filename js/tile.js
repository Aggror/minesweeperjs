﻿function Tile() {
    var self = this;
    this.mine = false;
    this.flag = false;
    this.clicked = false;
    this.checked = false;
    this.neighbours = 0;
    this.img;

    this.Draw = function (x, y, w, h) {
        ctx.drawImage(this.img, x, y, w, h);
    };
};