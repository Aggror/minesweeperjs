﻿function timestamp() {
    return window.performance && window.performance.now ? window.performance.now() : new Date().getTime();
}

function Timer() {
    this.timestamp;
    this.count;

    this.Reset = function()
    {
        this.timestamp = (new Date).getTime();
    }

    this.Start = function (count)
    {
        this.count = count;
        this.timestamp = (new Date).getTime();
    }

    this.Progress = function () {
        return ((new Date).getTime() - this.timestamp)/1000;
    }

    this.Busy = function ()
    {
        return this.Progress() < this.count;
    }


}