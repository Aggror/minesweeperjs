﻿function Button(text, xPos, yPos, width, height) {
    var self = this;
    this.position = { x: xPos, y: yPos };
    this.width = width;
    this.height = height;
    this.clicked = false;
    this.hovered = false;
    this.text = text;
    this.OnClicked;
    this.image;

    this.EventCheck = function (e) {
        var rect = cv.getBoundingClientRect();
        var mPos = {};
        mPos.x = e.x - rect.left;
        mPos.y = e.y - rect.top;

        //in button reach?
        if (mPos.x >= self.position.x &&
            mPos.x <= self.position.x + self.width &&
            mPos.y >= self.position.y &&
            mPos.y <= self.position.y + self.height)
        {
            self.OnClicked();
        }
    }

    cv.addEventListener('click', this.EventCheck);
}



Button.prototype.Destroy = function() {
    cv.removeEventListener('click', this.EventCheck);
}

Button.prototype.SetImage = function (image) {
    this.image = image;
}

Button.prototype.Draw = function() {
    //draw button 
    ctx.fillStyle = "#FFFF00";
    ctx.fillRect(this.position.x, this.position.y, this.width, this.height);

    //draw image if set
    if (this.image)
        ctx.drawImage(this.image, this.position.x, this.position.y, this.width, this.height);

    //text options
    var fontSize = 40;
    ctx.fillStyle = "#FF0000";
    ctx.font = fontSize + "px Verdana";

    //text position
    var textSize = ctx.measureText(this.text);
    var textX = this.position.x + (this.width/2) - (textSize.width / 2);
    var textY = this.position.y + (this.height / 2) + (fontSize / 2);

    //draw the text
    ctx.fillText(this.text, textX, textY);
}